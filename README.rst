Living Data Science Guidelines
==============================

This repository contains the guidelines and best practices for working
on data science projects: https://jhauh.gitlab.io/ds_handbook/

It is written in RST format so as to be:

- nicely presented
- plain text in order to be versionable
- exportable in many formats

Generating the document from source
-----------------------------------

To generate some nice static html which looks a bit like confluence,
run ``make html`` to generate the document from source.

If you intend to print the document, or use your browser's "Save to 
pdf" feature, then ``make singlehtml`` is a better option.

If you want to create a high quality pdf file, ``make latex`` will
generate tex files which can be converted to pdf by your favourite
latex compiler.

Contributing to the guidelines
------------------------------

These guidelines are open to new contributions, and welcome amendments,
changes, and extensions.  

The content of the guidelines is in the ``source`` folder, in a 
reasonably flat structure. To amend or extend an existing page, simply
edit it, test how it looks with ``make html``, and commit once happy.
To add new pages, add them to the source folder and then add them to
one of the ``toctree`` directives (see ``index.rst`` for an example).

