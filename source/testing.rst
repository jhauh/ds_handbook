Testing
=======

Motivation
----------

Good software engineering practices have been creeping into the realm of
data science for many years, and they are here to stay. Robust testing
is one such practice. As the mighty Joel Grus puts it, "Good software
engineering is the safest/easiest way to be (somewhat) confident that
your code works correctly and efficiently". Or in the words of the
almighty Hadley Wickham, "Testing your code can be painful and tedious,
but it greatly increases the quality of your code". 

General Standards
-----------------

Whilst the responsibility of testing a model going into production does
not fall on the data science team, establishing the correct framework
for those who inherit the code can streamline the process. Additionally,
tests can be an invaluable tool for data scientists to codify the
underlying assumptions, beliefs, and observations made during the
project, which future developers may not be aware of. For example, if
the model type chosen is only suitable when the input is not sparse due
to missing values, that assumption can easily be represented in a test,
which can be handed to others who need not understand the inner workings
of the algorithm. If the given method requires normality, that
requirement can be turned into a test. Therefore, these guidelines do
not enforce strict rules around unit tests or code coverage, but rather
encourage the use of testing as a tool to spot issues earlier, and make
development easier.

At a minimum, projects should aim to:

#. **Have a tests folder**
   
   Even if you plan on doing minimal testing, there are standardised
   places in your code base to keep tests. Testing software searches
   these places for your tests, and so do other developers. Resist the
   temptation to keep "tests" within the core code, and instead abstract
   them out into the tests folder. Using the :doc:`cookie cutter
   <codestructure>` for python projects handles a lot of this
   automatically. 

#. **Use common testing tools**
   
   There is no need to develop your own testing framework. `Testthat`_
   is the most popular and nicest testing package in the R ecosystem,
   and while there are plenty of options to choose from for Python,
   `pytest`_ is the recommended tool of choice, due to its ease of use
   and handy `pyspark extension`_.

#. **Distinguish between testing the *code* and testing the *data***

   Testing code commonly involves creating simple test cases and toy
   datasets, and checking the output is as expected. Testing data
   (whether that be input, intermediate, or the output of the code)
   usually means checking data types, accepted values, missings, and
   ensuring different statistics of the data are within reasonable
   ranges. Be explicit about what each test or script is testing, and be
   careful not to create circular loops where data to be tested is used
   to test code, and vice versa.


.. _Testthat: https://testthat.r-lib.org/
.. _pytest: https://docs.pytest.org/en/latest/
.. _pyspark extension: https://pypi.org/project/pytest-spark/