Technical Documentation
=======================

Motivation
----------

All models are wrong. Some models are useful. And a model which *only
you* can run, adapt, and refit is not a useful model. Business users,
managed services, and certainly the productionizing team need to be able
to do all of these things in order to use your model, and have it see
the light of day. Therefore, it is important to write high quality
documentation which shows how the code base should be used and extended.

While the :doc:`Code Documentation <codedocumentation>` section of these
guidelines provides structure around how to minimise the amount of
effort involved in keeping up-to-date documentation for the code itself,
this section offers guidance on how to provide a practical user manual
for future consumers.

Documentation Topics
--------------------

Installation
++++++++++++

Instructions on how to install the relevant dependencies is one of the
most important pieces of documentation to include in any technical spec.

Running the pipeline
++++++++++++++++++++

A typical modelling project should contain separate, modular code to:

#. **Clean input data**

   This involves handling basic data issues (data types, missing values
   etc.)

#. **Prepare data for modelling**
   
   Deriving and preparing input features for model training/execution. 
   There may be some overlap and combination with the first step.

#. **Train the model**
   
   Train an ML/statistical model and save it to disk.

#. **Score the model**
   
   Given a model and some prepared data, generate predictions/scores.

#. **Validate the model**

   Measure the performance or accuracy of a given model, by comparing 
   its predictions with the ground truth.

Since each of these steps may be productionized by a different team and
deployed in a different place, a good user guide should make explicit
how to run *each individual step*.  It may also be helpful to provide
instructions on how to run the entire pipeline, if relevant.

Running the tests
-----------------

Any and all tests should be executable by anyone who has access to the
code base. Providing instructions to do this - however seemingly trivial
- ensures those who go on to QA or extend the project can ensure their
changes do not break existing behaviours. 

Contributing
------------

Making explicit the conventions adopted for the development of the
project can cut down the time-to-contribute for new developers on the
project, or after the code base has been handed over. If the project
follows a specific style guide, git workflow, testing structure,
documentation approach, or any other convention, say so on a
"Contributing" page. Instructions on more practical details of how to
contribute (even if it's just "clone the repo, commit, and push") are
always appreciated, if only for the peace of mind they provide. 
