Version Control
===============

Motivation
----------

This document is to be used as a high-level guide to managing the source
code of your data science project with Git. Git is a powerful tool which
enables coherent and non-destructive collaboration, and allows for
flexible revision and selection of changes to the code. When used
effectively, it can significantly reduce the overhead of combining work
from multiple authors, on-boarding new contributors onto a project, and
smooth the hand-off of projects to production teams. While it is not the
only version control software available, it is undoubtedly the most
widely known and used tool for data science code bases, and therefore it
is the tool recommended here.

However, in order to fully realise the benefits of Git, there must be
co-ordination or agreement between collaborators. This co-ordination
must go beyond the core functionality of the tool, as a kind of social
contract between contributors. Suggested "contracts" are detailed below.

General Standards
-----------------

- **Use Git**
  
  Actually use it. Create a repository (from scratch, with our
  :doc:`Cookie Cutter <codestructure>`, or fork an existing one), put
  code in there, and push it to GitHub. Even if you're the sole
  contributor. It's worth it purely for the redundancy, but it's also
  invaluable to have an explicit log of the timeline of edits, so you
  can go back and see what changed when (and why).

- **Agree on a branching strategy**
  
  Suggested strategies are outlined below, they range from simple to
  complex. Once settled on, this is often documented in a ``Readme`` or
  ``Contributing`` file within the repo itself. This may evolve with the
  project; e.g. the addition of a second contributor may necessitate a
  more careful approach.

- **Know the basics**

  If you aren't already comfortable with ``add``, ``reset``, ``commit``,
  ``fetch``, ``pull``, ``push``, ``merge``, ``checkout``, or ``branch``,
  then get comfortable. There are `many`_ high-quality resources for
  `learning`_ and `playing`_ around with the commands.   Once you have
  mastered those, some of the fancier tricks like squashing and rebasing
  are worth learning to streamline your commit history, and are
  necessary for some branching strategies.

- **NO!tebooks**

  Data scientists frequently use Jupyter notebooks for exploration and
  analysis. However, while the ``.ipynb`` files they exist as are
  *technically* plain text (JSON-like), they are messy, can clog up your
  diffs and statuses, and can contain large images and personal data in
  the form of table views. For this reason, it is recommended not to
  commit 'messy' notebooks containing output. Instead, consider:
  
  - Removing all output before committing (either using the option in 
    the Jupyter UI or with a tool like `nbstripout`_
  - Adding a `pre-commit hook`_ to your repo to automatically perform 
    the above on commit
  - Convert notebooks to python scripts and commit those
  - Disallow notebooks (add to the repository's ``.gitignore``)

Branching Strategies
--------------------

The below strategies are reasonable approaches. They vary in terms of
overhead and benefit, but the overall question to ask is which one is
most appropriate for the project at hand.

- `Centralised`_: just commit to master

  Simple, straightforward, linear. Pull early, pull often to minimise 
  messy merge conflicts.

- `Feature branches`_: master is the meeting point, not the development 
  point
  
  Useful when contributors are developing on unrelated streams of work,
  as often happens in data science projects. Most development is
  committed to feature branches. Once a feature or work stream reaches a
  natural end point, it is merged back into master. This approach has
  the advantage of allowing contributors to work independently, and
  keeps the log tidy (and therefore useful).

- `Git Flow`_: develop is the new master, master is production

  This approach extends the idea of feature branches, and fleshes out
  some optional structure for maintaining an actively developed code
  base which is (or will be) in production. It has proven extremely
  popular, so much so that there are several different `tools`_ which
  aim to help users follow its philosophy. Although the overhead is the
  largest of the suggested strategies, it has the advantages of scaling
  well with parallel contributors and features, and producing beautiful
  git logs which paint a picture of the project's history.

Tips and tricks
---------------

Below are a collection of handy shortcuts which you may find useful in 
your git journey.

Using the log effectively
+++++++++++++++++++++++++

Add the following to your global ``.gitconfig`` for a nicer git log:

.. code-block:: bash

  [alias]
  lg = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)'

Which looks like this:

.. figure:: _static/gitlog.png

This view of the log is a great way to see an extended history of your 
repository, and looking at it often will encourage you to adhere to some
good Git practices, such as:

- Using branches to bundle related commits together
- Writing meaningful commit messages
- Keeping them snappy; if more detail is required, take a new 
  line/paragraph in the commit message (only the top line/header is 
  displayed here)
- Considering squashing several smaller commits into bigger more
  meaningful ones


.. _many: https://try.github.io/
.. _learning: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
.. _playing: https://learngitbranching.js.org/
.. _nbstripout: https://pypi.org/project/nbstripout
.. _pre-commit hook: https://github.com/fastai/fastai-nbstripout
.. _Centralised: https://www.atlassian.com/git/tutorials/comparing-workflows
.. _Feature branches: https://www.atlassian.com/git/tutorials/comparing-workflows
.. _Git Flow: https://nvie.com/posts/a-successful-git-branching-model/
.. _tools: https://github.com/nvie/gitflow