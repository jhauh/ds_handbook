Data Folder Structure
=====================

Motivation
----------

While every data science project is different, they often share common 
elements. For example, most projects involve:

#. Fetching/cleaning data
#. Exploration and analysis of that data
#. Fitting a model or models
#. Evaluation of the trained model(s)

In each of these steps, we require data, usually in successively evolved
forms. Organising this data in a logical, standardised way can help to
keep a project manageable, help new contributors to understand the
processes and pipelines involved, and ease the transition into
deployment to production.

General Standards
-----------------

Standard Structure
++++++++++++++++++

The below structure is a minimal framework for the data for DS projects.
Additional directories should be added where necessary. Each project 
should contain the following folders:

#. **Raw**: the data as delivered or pulled from source
  
   This should be treated as immutable, and can be thought of as the 
   foundations upon which your project is built.

#. **Clean**: tidied, filtered, or otherwise clean version of the data
   
   The line between cleaning and preparing is blurred, so don't worry 
   too much about it and just try to be consistent across data sets.

#. **Prepared**: data which is suitable for model building
   
   This is your "good" data, and is a nice place to store different 
   splits, e.g. train, validate, test.

#. **Scored**: data with predictions, and any evaluation
   
   Sometimes also named "Results", this is where you go to find scored 
   records, and any analysis/investigation of predictions.

#. **Models**: saved, loadable model files
   
   Serialised models go here. Optionally they may instead be stored in 
   the Prepared or Scored folder, if it makes sense to keep them
   alongside the relevant data/predictions.

Centralisation
++++++++++++++

In general, it is recommended that the data required for a project be
stored in a single location, as opposed to maintaining one version per
developer (usually in or near their local repo). Centralisation has the
advantages of avoiding keeping multiple copies of potentially large data
sets, scaling with additional developers, and being more data hygienic
generally.
