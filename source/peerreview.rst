Peer Review
===========

Motivation
----------

People make mistakes. Peer review is a system to catch and fix those
mistakes, and to validate and learn from each other's work. Establishing
a consistent approach for reviewing is helpful to guard against errors
and keep team members aligned. 

We divide peer review into two main forms:

Code Review
-----------

This type of review concerns inspecting code written to ensure it is of
high quality. It is recommended that any significant body of code
written be reviewed by at least one other team member. Reviewing
typically involves the following steps:

#. **Read the code**
   
   Is the purpose of the code clear? Does it conform to the surrounding 
   :doc:`Code Style <codestyle>`? Does it appear to behave as intended?

#. **Run the code**
   
   Being able to run the code somewhere other than the environment it
   was developed in is a crucial test. This is normally facilitated
   through Git; the reviewer pulls down the commit of the code in
   question, and ensures that it can be ran end-to-end. Any small
   resulting changes can be made by either the reviewer or the original
   contributor, and large changes or feedback should be given to the
   original contributor.

Modelling Review
----------------

This kind of review focuses less on the code itself, and more on
ensuring that the choices of approaches, methods, and algorithms used to
tackle a problem are sound. They are typically less frequent than code
reviews, with a larger set of reviewers, although this depends on the
project. The nature of modelling review is generally quite open, with
the following loose structure:

- The reviewee introduces the project/topic and provides context around 
  the given problem
- Any relevant modelling decisions are called out
- Those decisions are justified, scrutinised, and debated amongst the 
  group

Note that the term "modelling decisions" refers not only to
modelling-specific choices such as the choice between ML algorithms, but
rather any significant decision along the path of reaching the project's
objective. For example, some common (but non-exhaustive) topics of
discussion include:

- Which data source(s) to use to build the model
- Which fields/features are safe to include
- Choice of response variable
- Which model architecture is most suitable e.g. GLM v Forest
- How the model should be evaluated
- The cost-benefit of investing significant time into a certain aspect 
  of the process

It is normal for the reviewee to provide supporting analysis, plots, or
relevant business requirements to help justify any decisions made. In
terms of the timing of modelling review, it can be helpful both at the
early and later stages of a project. Earlier reviews can focus a project
and the approach taken, whereas later reviews can be used to reflect and
learn from the project, and to suggest potential improvements or tweaks.