Coding Standards
================

Writing code is undoubtedly a creative process; the notion of building
something or solving a problem in your own way is an alluring prospect
for all data scientists. Finding the right balance of performance,
clarity, and expressiveness within the confines of your problem space is
the archetypal task programmers are faced with every day.

These guidelines are not intended to stifle the follower's creative
streak, or restrict their freedom to solve the problem in the way they
think is best. Rather, they are tools intended to augment the follower's
ability to tackle problems, and to provide common building blocks so
that bigger, better solutions can be built, reused, and collaborated on
more effectively. 

Each section contains the motivation for why the guidelines therein
exist, as well as the general principles to follow. Content is mostly
linked to externally, with some high-level descriptions provided. 

.. toctree::

   codestructure
   codestyle
   versioncontrol
   codedocumentation
   testing
   peerreview