Code Documentation
==================

Motivation
----------

A project is only as good as its documentation. If another developer
cannot understand or extend your work, they will rewrite it. If a user
cannot figure out how to run your code, they will not run it.
Documentation is the face of the project, and can have a significant
effect on how it is perceived. Maintaining good documentation is also
useful for your own sake, to help remind you what you were thinking when
you initially wrote the code. These factors are especially important for
data science projects, where code often contains combinations of heavy
mathematics, modelling decisions, and business logic.

General Standards
-----------------

Keeping documentation up-to-date in a constantly evolving code base can
be challenging. For this reason, the idea of "self-documenting code" has
become popular, and many tools exist to help with this. This approach
keeps the documentation alongside the code, mainly in the form of
docstrings, minimising the distance between code and documentation which
describes the code. This has the advantage of only having to edit a
single file, and making it more obvious when the docs become outdated.
Below are some recommended tools for getting the most out of the effort
spent documenting. 

Python
++++++

For documenting python projects, `Sphinx`_ is the most popular tool. It
crawls your code base, pulls module/class/method/function docstrings and
signatures, and creates beautiful API documentation. This usually takes
the form of static HTML which can be hosted online (e.g. on the
repository's GitHub Pages page), but can also be used to create PDF
manuals and several other formats. This documentation can be augmented
with additional comments, sections, pages and chapters of more explicit
written documentation, and can incorporate Jupyter notebooks, which can
be useful for example usage and walk-throughs. 

Sphinx is currently the default tool in the DS Cookie Cutter, so all you
need to do to make use of it is '``make html``'. The RST file format
used to generate this documentation is like Markdown, but more
expressive. However, Markdown can be used if preferred. See the `Cookie
Cutter repository`_ for more information on usage.

Another tool for this task is `Mkdocs`_, a Markdown-based tool for
static documentation generation. Both are good options

R 
+++

`Roxygen`_ is the most popular and best-supported tool of choice.

.. _Sphinx: http://www.sphinx-doc.org/en/master/
.. _Cookie Cutter repository: https://github.com/fluffactually/cookiecutter-ds-python
.. _Mkdocs: https://www.mkdocs.org/
.. _Roxygen: https://cran.r-project.org/web/packages/roxygen2/vignettes/roxygen2.html