Code Style
==========

Motivation
----------

This document is to be used as a high-level guide to coding styles and
standards. The importance of coding standards lies in the ability of
others to read and understand your code. This is particularly important
for code which will be used in solutions in production. Code which is
difficult to read and understand is difficult to manage. As such, code
which does not comply to the standards will not be promoted to
production where the managed services team will need to monitor and
maintain it.

Overview
--------

Most of our data science projects to date have been carried out in R or
Python. Therefore, these guidelines include language-specific
suggestions below. However, some general advice is also provided which
applies to R, Python, and to any other languages a project could be
carried out in, like Julia or SQL for example. No specific style guides
are detailed here, instead a selection of options are linked to for you
to consider, with some sensible defaults. Anything linked is deemed
worthwhile reading, with a few documents marked as "**must-reads**".

General Standards
-----------------

- If starting a project or creating a repository/code base from scratch,
  pick a coding style and try to stick to it
- If joining or picking up a project, adhere to the style of the 
  existing code
- Know when to ignore a guideline. Some good reasons for doing so:
  
  #. When applying the guideline would make the code less readable, even 
     for someone who is used to reading code that follows the guideline
  #. To be consistent with surrounding code that also breaks it (maybe 
     for historic reasons) -- although this is also an opportunity to 
     clean up someone else's mess
  #. Because the code in question predates the introduction of the 
     guideline and there is no other reason to be modifying that code
  #. When the code needs to remain compatible with older versions of 
     languages/packages that don't support the feature recommended by 
     the style guide

- It does no harm to explicitly state what style the projects attempts 
  to follow, and can be helpful for new contributors. Often this is 
  detailed in a ``README`` or ``Contributors`` file, or other accessible 
  documentation
- Using automatic *linter* plugins for your editor/IDE of choice can 
  make it easy to get in the habit of writing neat code, but exercise
  caution in using automatic code *formatters*

Python Standards
----------------

There are many different style guides for Python. Underpinning all of
them is the notion of "idiomatic code", also commonly referred to as
"Pythonic" code. If you ask Python programmers what they like most about
Python, they will often cite its high readability. Indeed, a high level
of readability is at the heart of the design of the Python language,
following the recognized fact that code is read much more often than it
is written. One reason for the high readability of Python code is its
relatively complete set of Code Style guidelines and “Pythonic” idioms.
When a veteran Python developer (a Pythonista) calls portions of code
not “Pythonic”, they usually mean that these lines of code do not follow
the common guidelines and fail to express its intent in what is
considered the best (hear: most readable) way.

General concepts can be found at the `Hitchhiker's guide to Python`_,
and can be thought of as the foundations underpinning all of the formal
style guides listed below. **This one is a must-read**.

Style Guides
++++++++++++

#. `PEP 8`_: the de facto code style guide for Python
   
   The majority of the Python community does their best to adhere to the
   guidelines laid out within this document. There are many tools,
   packages, and extensions which can help to ensure your code conforms
   to this standard (e.g. `pycodestyle`_, `flake8`_, `SublimeLinter`_).
   Unless you have a reason not to, this is a good default choice. Since
   many of the alternative guides are deviations from this standard,
   **this one is also a must-read**.

#. `PEP 8++`_: like PEP 8, but wider

   One of the main critiques of PEP 8 is its relatively narrow width (79
   columns wide for code, 72 for comments). These limits were primarily
   chosen so that code would be readable on lower-resolution screens
   without line-wrapping or horizontal scrolling, or to allow multiple
   windows side-by-side. However, in the age of widescreen monitors this
   is less of a concern, and there is `analysis which suggests`_ that a
   width of ~90 is optimal for readability. The linked style guide
   enhances PEP 8 by increasing the maximum line width, and is worth a
   try if you find yourself using long variable names or deeply nested
   structures.

#. `Black`_: opinionated code formatter, "handles style so you can focus
   on the code"

   The idea behind Black is that all contributors would use it to ensure
   complete consistency between code style, meaning no awkward
   back-and-forth diffs between you and your colleague's code formatter.
   It has a number of plugins for different editors and tools to ensure
   compatibility. The style guide underpinning the tool is described as
   "*a strict subset of PEP 8*", and is a reasonable option in its own
   right.

#. `FastAI`_: Concise style geared towards understanding math-heavy code

   The biggest departure from PEP 8 in this list, this style optimizes
   on fitting everything encapsulating one semantic idea in a single
   screen of code. It recommends large line widths, short variable
   names, and general terseness. If your project makes heavy use of
   ``TensorFlow`` or other deep learning-related libraries, this
   guideline is worth considering. 

R Standards
-----------

In general, R code is more relaxed than python when i comes to defined
standards for style. This is partly due to the more academic-skewed
community of R users (as opposed to traditional software engineer
circles), and the wonderful Lisp-y characteristics of the language which
make creating domain-specific languages, or DSLs, a breeze, which in
turn makes it more difficult to come up with overarching code style
guidelines. In spite of this, two style guides have proven popular and
are commonly recommended. Both are great options, so just pick one. When
working with DSLs (e.g. ``magrittr``, ``data.table``), adhere to the
package's guidelines on style.  

Style Guides
++++++++++++

#. `Google`_:
   
   Provides a reasonable and consistent set of rules for R code. 
   Uncontroversial, but worth a read.  

#. `Tidyverse`_: opinionated evolution of Google's style to cover common
   tidyverse packages
   
   If you use pipes a lot, or dplyr a lot, or any of the other tidy 
   packages, this is a good choice.


.. _Hitchhiker's guide to Python: https://docs.python-guide.org/writing/style/#general-concepts
.. _PEP 8: https://pep8.org/
.. _pycodestyle: https://pypi.org/project/pycodestyle/
.. _flake8: https://pypi.org/project/flake8/
.. _SublimeLinter: https://packagecontrol.io/packages/SublimeLinter-pycodestyle
.. _PEP 8++: http://docs.python-requests.org/en/master/dev/contributing/#kenneth-reitz-s-code-style
.. _analysis which suggests: https://www.youtube.com/watch?v=wf-BqAjZb8M&feature=youtu.be&t=260
.. _Black: https://black.readthedocs.io/en/stable/the_black_code_style.html
.. _FastAI: https://docs.fast.ai/dev/style.html#style-guide
.. _Google: https://google.github.io/styleguide/Rguide.xml
.. _Tidyverse: https://style.tidyverse.org/