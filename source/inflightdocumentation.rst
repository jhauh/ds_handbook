In-Flight Documentation
=======================

Motivation
----------

Throughout the project, it can be helpful to keep a track of certain
decisions, directions, events, and contacts. While tools like JIRA are
invaluable for tracking progress on development work, it is not well
suited to storing "steady state" information, which is referred to only
infrequently and as a reminder of what has happened so far. For this
reason, some separate documentation is recommended.


Suggested Documents
-------------------

Data Sources Log
++++++++++++++++

A row-per-table or row-per-database file containing practical
information about the data sources relevant to the project. This may
include information on where the data can be found, who owns or provides
the data, the location of any data dictionaries provided, when it was
refreshed/loaded, and any relevant comments or description. A
standardised format is given below:

====  ===========  ======  ====  ====  ====  ==========  ===========  =========  ====  ===========  =======  ========
Name  Description  Source  Name  Data  Type  Time Range  Provided By  Provision  Date  Loaded Date  Comment  Priority
====  ===========  ======  ====  ====  ====  ==========  ===========  =========  ====  ===========  =======  ========


Decision and Statements Log
+++++++++++++++++++++++++++

A file to document any important decisions and statements made during 
model development, which are important to know in case of a handover to 
someone else and when productionizing. A standardised format is given 
below:

==========  ==================  ====  =======
Decision #  Decision/Statement  Date  Made By
==========  ==================  ====  =======


Model Feature Log
+++++++++++++++++

For model-heavy projects, it can be useful to include a list of features
included in the model. This can be used to include definitions, upstream
data sources, levels, and incremental performance impact of each
feature.