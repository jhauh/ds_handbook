Project Documentation
=====================

Motivation
----------

The more consistent the output of data science projects, the more
streamlined the productionizing process can be. While every project is
different, these guidelines define some standard outputs for projects. 

Standard Outputs
----------------

All data science projects should produce the following outputs at a
minimum:

Code repository  
   A beautiful, installable, extensible python package in a git
   repository based on the reference data science cookie cutter
   template. As described in :doc:`Coding Standards <codingstandards>`.

:doc:`Technical Documentation <technicaldocumentation>`  
   A user guide on how to run the code provided.

Iterative Outputs
-----------------

:doc:`In-Flight Documentation <inflightdocumentation>`
   Demo slides, decision logs, and other documents created iteratively
   throughout the project.

:doc:`Known Issue Documentation <knownissuedocumentation>`
   A log of challenges, compromises, and blockers identified over the
   course of the project.

.. toctree::
   :hidden:

   technicaldocumentation
   inflightdocumentation
   knownissuedocumentation
