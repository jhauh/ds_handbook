Data Standards
==============

Data science projects ingest data, process data, and produce data. These guidelines suggest standard practices for handling data which aim to make clear each dataset's lineage and purpose, and provide consistency across projects.

.. toctree::
   :maxdepth: 2
   :caption: Data:

   datafolderstructure
   outputguidelines