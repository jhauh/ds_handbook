Code Structure
==============

Motivation
----------
While every data science project is different, they often share common 
elements. For example, most projects involve:

#. Fetching/cleaning data
#. Exploration and analysis of that data
#. Fitting a model or models
#. Evaluation of the trained model(s)

All of these steps involve writing code. This code is a deliverable of 
any project, and has certain requirements which typically do not vary 
by project. Project code needs to be:

- *Reproducible*, so users can obtain the same results
- *Organised* sensibly, so users can run it easily
- *Documented*, so users can understand it
- *Tested*, so changes which break things are identified quickly

Given these similarities between projects, we have created a "cookie
cutter" repository template, which provides an organised structure for
data science (python) project code. This has the advantage of
quick-starting new projects (it's faster to clone a cookie cutter than
it is to decide upon and create a repository which caters to all of the
above requirements), and aids in ensuring consistency between projects,
which reduces understanding and on-boarding time for developers and
users. It encourages developers to create a Python module or submodules,
so the code can easily be imported and installed elsewhere. 

For further discussion of the advantages of cookie cutter projects, see
the original `data science cookie cutter project page`_ upon which our
template is based. 

Usage
-----

The `GitHub repo`_ contains instructions on how to install and use the
cookie cutter to start your own project. Once you have created a
repository in its image, it's yours to do with as you see fit. If you
don't require testing, delete the testing folder. If your project makes
more sense to be built in two python packages instead of one, make it
so. This tool aims to be a common starting point, not an ending point.

Default Structure
-----------------

The adopted structure is a cut-down version of the `original DS cookie
cutter repo`_ from DrivenData. Compare the below directory structure to
that on the linked repository for a full set of changes, but at a high
level the main differences are:

- Uses `Poetry`_ instead of conda to manage environments & dependencies
- Data is assumed to be centralised (i.e. stored in one place instead of
  keeping a copy in every user's local repo). See the 
  :doc:`datafolderstructure` page for more on this
- Fewer folders (less testing, condensed documentation)

::

  Project Organisation
  --------------------
  
  ├── LICENSE
  ├── README.rst
  ├── docs
  │   ├── Makefile
  │   ├── README.rst
  │   ├── build
  │   └── source
  │       ├── _static
  │       ├── _templates
  │       ├── api.rst
  │       ├── conf.py
  │       ├── contributing.rst
  │       ├── index.rst
  │       ├── install.rst
  │       ├── structure.rst
  │       └── usage.rst
  ├── pyproject.toml
  ├── run
  │   ├── a1_create_data.py
  │   ├── b1_print_data.py
  │   ├── config
  │   │   ├── a1_create_data.config
  │   │   └── b1_print_data.config
  │   └── run_pipeline.py
  ├── src
  │   └── {{ cookiecutter.package_name }}
  │       ├── __init__.py
  │       └── config.py
  └── test
      ├── README.rst
      └── test_jobconfig.py


.. _data science cookie cutter project page: http://drivendata.github.io/cookiecutter-data-science
.. _GitHub repo: https://github.com/fluffactually/cookiecutter-ds-python
.. _original DS cookie cutter repo: https://github.com/drivendata/cookiecutter-data-science
.. _Poetry: https://python-poetry.org