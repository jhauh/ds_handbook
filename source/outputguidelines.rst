Output Guidelines
=================

Motivation
----------

The output of a data science project can take many forms; often it is a
model or measurement of some KPI(s) of interest, but it can also be a
set of predictions, an analytical report, or some other output data to
be consumed downstream. If this output is difficult to understand or
consume, then in most cases it will not be understood or consumed. This
can in turn lead to the output of the project being slow to act on or
productionize, or discarded entirely. 

General Standards
-----------------

The following recommendations aim to lower the barriers around 
consumption of the project's output.

- **Use plain-text, human readable formats where possible**  

  Consumers of your project's output who will make decisions based on
  that output need to be able trust it. To do that, it helps to be able
  to easily inspect the output and perform some visual and manual sanity
  checks. Where possible, flat, plain-text files should be used to hand
  over data. Use reasonable delimiters and name the file appropriately
  (i.e. ``.csv`` for comma-delimited, ``.psv`` for pipe-delimited,
  ``.tsv`` for tab-delimited, ``.dat`` for all others). Provide
  meaningful column names. If you can head the file or open it in Excel
  or a text editor, you're on the right track.  

- **Provide a data dictionary**  

  This can take the form of a simple header-description file in any
  format, or an example template file which consumers can use to build
  and test their consumption pipelines. The level of detail provided
  should be proportional to the importance of the output.