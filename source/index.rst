Data Science Project Guidelines
===============================

No two data science projects are the same. However, generally any data
science project will aim to deliver:

#. A high-quality model
#. Stable and accurate predictions
#. Reproducibility of the above through robust code

These guidelines provide a framework for delivering the above in a
reasonably open and standardised fashion.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   codingstandards
   datastandards
   projectdocumentation
