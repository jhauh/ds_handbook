Known Issue Documentation
=========================

Motivation
----------

Inevitably, there will be problems, blockers, and other issues which
arise as the project progresses. Some of these hurdles will be
manageable, but others will be out of the control and scope of the
project at hand. In the case of the latter, it is useful to document
these issues in order to make others aware, to prioritise work for
upstream teams, and to update plans and priorities accordingly. 

Guidelines
----------

Smaller issues and blockers should be recorded and handled in the usual
way in JIRA. If an issue is too big, out of the team's control, or
fundamentally limiting to the project, then it should be recorded as a
known issue. It is suggested that this be done on Confluence, preferably
as a child page of  the Project's page. This gives maximum visibility to
known issues, and is the easiest way to link those who can help to the
problem. However, if a more structured approach is necessary, a shared
Excel file can be used instead. This can be more useful if it is
necessary to document many aspects of the same issue, such as who is
responsible, downstream consequences, workarounds, model performance
impacts, and any fixes or resolutions.

Examples
--------

Some possible issues which may arise during a project are given below, 
along with some potential workarounds or compromises.

Data Issues
+++++++++++

- Upstream data is missing, either within tables, rows, or columns, 
  cannot be reliably be used for modelling
- Upstream data sources are inconsistent, one must be chosen as the 
  ground truth for the purposes of the project
- Upstream data source is unpredictably inconsistent over time, (e.g. 
  labels, columns change), affected data cannot be used for modelling

Modelling Issues
++++++++++++++++

- Label or response field is demonstrably incorrect or inaccurate, must
  find or define alternate labels
- Upstream data is fundamentally censored (i.e. survivor bias etc.) 
- Preferred model type cannot be used due to technical constraints, 
  another model type must be used
- Data is potentially forward looking, cannot be used in current form 

